package ito.poo.clases;

import java.time.LocalDate;
import java.util.ArrayList;

public class Prenda {
	
	private int modelo;
	private String tela;
	private float costoProduccion;
	private String genero;
	private String temporada;
	private ArrayList<Lote> lotes = new ArrayList<Lote>();
	/**********************************************************/
	public Prenda() {
		super();
	}
	
	public Prenda(int modelo, String genero, String temporada, String tela, float costoProduccion) {
		super();
		this.modelo = modelo;
		this.genero = genero;
		this.temporada = temporada;
		this.tela = tela;
		this.costoProduccion = costoProduccion;
	}
	/**********************************************************/
	public int getModelo() {
		return modelo;
	}
	
	public void setModelo(int modelo) {
		this.modelo = modelo;
	}

	public String getTela() {
		return tela;
	}

	public void setTela(String tela) {
		this.tela = tela;
	}

	public float getCostoProduccion() {
		return costoProduccion;
	}

	public void setCostoProduccion(float costoProduccion) {
		this.costoProduccion = costoProduccion;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getTemporada() {
		return temporada;
	}

	public void setTemporada(String temporada) {
		this.temporada = temporada;
	}

	/**********************************************************/
	public void addLote(int a, int b, LocalDate c) {
			Lote p = new Lote(a, b, c);
				this.lotes.add(p);
	}
	
	public Lote getLote(int p){
			Lote P = null;
				if (p > 1 || p < this.lotes.size())
					P = this.lotes.get(p - 1);
						return P;
	}
	/**********************************************************/
	public float costoxLote(float costoxUnidad) {
			float p = 0;
				return p;
	}
	/**********************************************************/
	@Override
	public String toString() {
		return "Prenda [modelo=" + modelo + ", genero=" + genero + ", temporada=" + temporada + ", "
				+ "tela=" + tela + ", costoProduccion=" + costoProduccion + " lotes=" + lotes + "]";
	}
	/**********************************************************/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(costoProduccion);
		result = prime * result + ((genero == null) ? 0 : genero.hashCode());
		result = prime * result + ((lotes == null) ? 0 : lotes.hashCode());
		result = prime * result + modelo;
		result = prime * result + ((tela == null) ? 0 : tela.hashCode());
		result = prime * result + ((temporada == null) ? 0 : temporada.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Prenda other = (Prenda) obj;
		if (Float.floatToIntBits(costoProduccion) != Float.floatToIntBits(other.costoProduccion))
			return false;
		if (genero == null) {
			if (other.genero != null)
				return false;
		} else if (!genero.equals(other.genero))
			return false;
		if (lotes == null) {
			if (other.lotes != null)
				return false;
		} else if (!lotes.equals(other.lotes))
			return false;
		if (modelo != other.modelo)
			return false;
		if (tela == null) {
			if (other.tela != null)
				return false;
		} else if (!tela.equals(other.tela))
			return false;
		if (temporada == null) {
			if (other.temporada != null)
				return false;
		} else if (!temporada.equals(other.temporada))
			return false;
		return true;
	}
	
	public int compareTo(Prenda arg0) {
		int r=0;
		if (this.modelo!=((Prenda) arg0).getModelo())
			return  this.modelo>((Prenda) arg0).getModelo()? 1:-1;
		else if (!this.genero.equals(arg0.getGenero()))
			return this.genero.compareTo(arg0.getGenero());
		else if (!this.temporada.equals(arg0.getTemporada()))
			return this.temporada.compareTo(arg0.getTemporada());
		else if (!this.tela.equals(arg0.getTela()))
			return this.tela.compareTo(arg0.getTela());
		return r;
	}
}