package ito.poo.clases;

import java.time.LocalDate;

public class Lote {

	private static Object Fecha;
	private int numLote;
	private int numPiezas;
	private LocalDate fecha;
	/**********************************************************/
	public Lote() {
		super();
	}
	public Lote(int numLote, int numPiezas, LocalDate fecha) {
		super();
		this.numLote = numLote;
		this.numPiezas = numPiezas;
		this.fecha = fecha;
	}
	/**********************************************************/
	public int getNumLote() {
		return numLote;
	}

	public void setNumLote(int numLote) {
		this.numLote = numLote;
	}

	public int getNumPiezas() {
		return numPiezas;
	}

	public void setNumPiezas(int numPiezas) {
		this.numPiezas = numPiezas;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	/**********************************************************/
	public float costoProduccion(Prenda P) {
		float p=0;
			p = P.getCostoProduccion() * this.numPiezas;
		return p;
	}

	public float montoProduccionxlote(Prenda P) {
		float p=0;
			p = costoProduccion(P) * 0.15F;
		return p;
	}

	public float montoRecuperacionxpieza(Prenda P) {
		float p=0;
			p = P.getCostoProduccion() * 0.5F;
		return p;
	}
	/**********************************************************/
	@Override
	public String toString() {
		return "(Numero de lote: " + numLote + ", Numero de piezas: " + numPiezas + ", Fecha: " + fecha + ")";
	}
	/**********************************************************/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result + numLote;
		result = prime * result + numPiezas;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lote other = (Lote) obj;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (numLote != other.numLote)
			return false;
		if (numPiezas != other.numPiezas)
			return false;
		return true;
	}
	public int compareTo(Lote arg0) {
		int r = 0;
		if (this.numLote != arg0.getNumLote())
			return this.numLote > arg0.getNumLote() ? 1 : -1;
		else if (this.numPiezas != arg0.getNumPiezas())
			return this.numPiezas > arg0.getNumPiezas() ? 2 : -2;
		else if (!Lote.Fecha.equals(arg0.getFecha()))
			return ((LocalDate) Lote.Fecha).compareTo(arg0.getFecha());
		return r;
	}

}